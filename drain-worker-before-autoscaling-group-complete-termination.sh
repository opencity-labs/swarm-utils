#!/usr/bin/env bash


# See Description in
#   https://gitlab.com/opencity-labs/ops/-/wikis/swarm-workers-autoscaling?edit=true
# Code in
#   https://gitlab.com/opencity-labs/swarm-utils/-/blob/main/drain-worker-before-autoscaling-group-complete-termination.sh
#
# This script should be in a frequent cron job in a docker swarm
#
# 1. We cointinuosly read the SQS queue to read if messages comes from
#    autoscaling lyfecycle hook
# 2. If message arrive we drain the worker then
# 3. Remove the message from the queue

AUTOSCALING_GROUP_NAME='ark-workers'
SQS_QUEUE_URL='https://sqs.eu-west-1.amazonaws.com/814614828974/ark-workers-autoscaling-in'

# Enable debug by uncommenting:
# set -ex

receive_messages() {
  aws sqs receive-message --queue-url $SQS_QUEUE_URL
}

get_instance_id_from_message() {
  echo "$1" | jq -r '.Body' | jq -r '.detail.EC2InstanceId'
}

get_receipt_handle_from_message() {
  echo "$1" | jq -r '.ReceiptHandle'
}


assure_message_is_appropriate() {
  autoscaling_group_name=$(echo "$1" | jq -r '.Body'  | jq -r '.detail.AutoScalingGroupName')
  if [ "$AUTOSCALING_GROUP_NAME" != "$autoscaling_group_name" ]; then
    # echo "AutoScalingGroupName in message is not: $AUTOSCALING_GROUP_NAME, so we ignore the message"
    echo -n "no"
  else
	echo -n "yes"
  fi
}

delete_message() {
  receipt_handle=$(echo "$1" | jq -r '.ReceiptHandle')
  aws sqs delete-message --queue-url $SQS_QUEUE_URL --receipt-handle "$receipt_handle"
}

get_instance_name_by_id() {
  aws ec2 describe-instances \
  --instance-ids "$1" \
  --query 'Reservations[].Instances[].Tags[?Key==`Name`].Value | [0]' \
  --output text
}

drain_worker_by_hostname() {
    docker node update --availability drain "$1"
}

slack::message()
{
  # wee need credentials in /etc/slacktee.conf
  local title=$1
  local msg=$2
  slacktee=$(command -v slacktee)
  if [[ -n $slacktee ]]; then
    echo "$msg" | $slacktee --plain-text --channel info --attachment good \
      --title "$title" --no-output 2> /dev/null
  fi
}


# check script requirements
for i in jq curl slacktee aws docker; do
  if [[ -z $(command -v $i) ]]; then
    die "Command '${i}' not available, cannot continue"
  fi
done


region=$(curl --silent http://169.254.169.254/latest/dynamic/instance-identity/document | grep "region" | awk -F '"' '{print $4}')
if [[ ! -f ~/.aws/config ]]; then
  [[ ! -d ~/.aws ]] && mkdir ~/.aws
  echo -e "[default]\nregion = $region\n" > ~/.aws/config
fi


# Inizio

messages=$(receive_messages)

if [ -z "$messages" ]; then
  # no message
  exit
fi

number_of_messages=$(echo "$messages" | jq '.Messages | length ')

# Iterate from 0 to the number of messages minus 1
for ((i=0; i<number_of_messages; i++)); do
	message=$(echo "$messages" | jq -r ".Messages[$i]")
	instance_id=$(get_instance_id_from_message "$message")
	echo "Received message for Instance_id: $instance_id"

	# Check if the instance ID starts with "i-"
	if [[ "$instance_id" =~ ^i- ]]; then
		message_is_appropriate=$(assure_message_is_appropriate "$message")
		if [ "$message_is_appropriate" != "yes" ]; then
			echo "non appropriate message received: so ignoring it"
		else
			instance_name=$(get_instance_name_by_id "$instance_id")
			if docker node ls | grep -q "$instance_name"; then

			slack::message "Autoscaling $AUTOSCALING_GROUP_NAME lifecycle hook" "Received lifecycle hook terminate for instance $instance_name, so executing drainining"

			drain_worker_by_hostname "$instance_name"
			delete_message "$message"
			else
			echo "instance $instance_name not in Swarm, so no action but I remove the message from the queue"
			delete_message "$message"
			fi
		fi
	else
	  echo "cannot get instance_id, so ignoring the message"
	fi
done
