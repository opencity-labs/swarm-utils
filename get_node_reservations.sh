#!/bin/bash

# Output total of reserved CPU and RAM for
# running tasks in Docker swarm node


set -e
node=$1
second_parameter=$2
third_parameter=$3
script_name=$(basename "$0")

if [ -z "$node" ]; then
	echo "Usage:"
    echo "$script_name <NODE>"
	echo
	echo "To consider all running tasks, not only where the task has a constrain on the node:"
	echo "$script_name <NODE> running"
	echo
	echo "To calculate also from service template (it should be the same)"
	echo "$script_name <NODE> running services"
    exit 1
fi

total_ram=0
total_cpu=0

total_ram_limit=0
total_cpu_limit=0

# get all tasks for a node
tasks=$(docker node ps --filter "desired-state=running" $node --format "{{.ID}}")
# we can either check tasks (running services) or check service template definition of the running tasks
for task in $tasks
do
    inspect=$(docker inspect $task)
    node_constraint=$(echo $inspect | jq -r  '.[].Spec.Placement' | jq -r '.Constraints[0]' | awk '{print $3}')
	# we skip if the constrain is not present
    if [ ! "$node_constraint" = "$node" ] && [ ! "$second_parameter" = "running" ]; then
        continue
    fi
    stack_name=$(echo $inspect | jq '.[].Spec.ContainerSpec.Labels."com.docker.stack.namespace"')

    ram=$(echo $inspect | jq '.[].Spec.Resources.Reservations.MemoryBytes')
    if [[ "$ram" == "null" ]]; then
      ram=0
    fi
    # divide the number of bytes by 1,073,741,824 (which is 1024^3).
    # This is because 1 kilobyte is 1024 bytes, 1 megabyte is 1024 kilobytes, 1 gigabyte is 1024 megabytes, and so on.
    ram=$(echo "scale=2; $ram / 1073741824" | bc)

    ram_limit=$(echo $inspect | jq '.[].Spec.Resources.Limits.MemoryBytes')
    if [[ "$ram_limit" == "null" ]]; then
      ram_limit=0
    fi
    # divide the number of bytes by 1,073,741,824 (which is 1024^3).
    # This is because 1 kilobyte is 1024 bytes, 1 megabyte is 1024 kilobytes, 1 gigabyte is 1024 megabytes, and so on.
    ram_limit=$(echo "scale=2; $ram_limit / 1073741824" | bc)

    cpu=$(echo $inspect | jq '.[].Spec.Resources.Reservations.NanoCPUs')
    if [[ "$cpu" == "null" ]]; then
      cpu=0
    fi
    # In Docker Swarm, CPU reservations are measured in NanoCPUs. One CPU is equivalent to 1000000000 (one billion) NanoCPUs.
    # Therefore, to convert NanoCPUs to CPU, you simply need to divide the number of NanoCPUs by 1000000000.
    # total_cpu_in_cpu=$(echo "scale=2; $total_cpu / 1000000000" | bc)
	cpu=$(echo "scale=2; $cpu / 1000000000" | bc)

    cpu_limit=$(echo $inspect | jq '.[].Spec.Resources.Limits.NanoCPUs')
    if [[ "$cpu_limit" == "null" ]]; then
      cpu_limit=0
    fi
    cpu_limit=$(echo "scale=2; $cpu_limit / 1000000000" | bc)

	service=$(echo $inspect | jq '.[].ServiceID')
	# Remove the double quotes at the beginning and end of the string
    service="${service#\"}"
    service="${service%\"}"
    service_name=$(docker service inspect $service --format='{{ .Spec.Name }}')

    # echo "Task.ID: $task Service.ID: $service RAM: $ram  CPU: $cpu Stack: $stack_name: Service: $service_name"
	echo "RAM reservation(limit): $ram ($ram_limit)  CPU reservation(limit): $cpu ($cpu_limit) Stack: $stack_name: Service: $service_name"

	services="$services $service"
	total_ram=$(echo "$total_ram + $ram" | bc)
    total_cpu=$(echo "$total_cpu + $cpu" | bc)
    total_ram_limit=$(echo "$total_ram_limit + $ram_limit" | bc)
    total_cpu_limit=$(echo "$total_cpu_limit + $cpu_limit" | bc)


done

echo
echo "Node: $node"
echo "Total reserved RAM: $total_ram GB (limit $total_ram_limit GB)"
echo "Total reserved CPU: $total_cpu CPU (limit  $total_cpu_limit CPU)"
echo

if [ ! "$third_parameter" = "services" ]; then
  exit
fi


echo "Now we get the same but calculating from the service template file"
echo "(we should get the same result)"
echo
total_ram=0
total_cpu=0

# docker service ls --filter "node.role==worker"
# services=$(docker service ls -q)

for service in $services
do
	inspect=$(docker service inspect $service)
	name=$(echo $inspect | jq '.[].Spec.Name')
    stack_name=$(echo $inspect | jq '.[].Spec.ContainerSpec.Labels.com.docker.stack.namespace')

	ram=$(echo $inspect | jq '.[].Spec.TaskTemplate.Resources.Reservations.MemoryBytes')
	if [[ "$ram" == "null" ]]; then
      ram=0
    fi
    ram=$(echo "scale=2; $ram / 1073741824" | bc)

	cpu=$(echo $inspect | jq '.[].Spec.TaskTemplate.Resources.Reservations.NanoCPUs')
    if [[ "$cpu" == "null" ]]; then
      cpu=0
    fi
    cpu=$(echo "scale=2; $cpu / 1000000000" | bc)
	name=$(echo $inspect | jq '.[].Spec.Name')
	echo "Service: $name ($service)  RAM: $ram  CPU: $cpu"
    echo "Service.ID: $service RAM: $ram  CPU: $cpu Name: $name"

 total_ram=$(echo "$total_ram + $ram" | bc)
 total_cpu=$(echo "$total_cpu + $cpu" | bc)
done

echo
echo "Node: $node"
echo "Total reserved RAM: $total_ram GB"
echo "Total reserved CPU: $total_cpu CPU"

